//Task1
function comparingResult(x, y) {
    let result;
    if (x < y) {
        result = x + y;
    } else if (x > y) {
        result = x - y;
    } else {
        result = x * y;
    }
    return result;
}

//Task2
function sumOfTwoArraysValues(arr1, arr2) {
    try {
        const arrToSum = arr1.concat(arr2);

        let numOr0 = value => isNaN(value) ? 0 : value;
        const result = arrToSum.reduce((accumulator, currentValue) => numOr0(accumulator) + numOr0(currentValue), 0);
        return result;
    } catch (e) {
        return e;
    }
}

//Task3
function countTrue(arr) {
    const arrOfTrues = arr.filter(element => element === true);
    return arrOfTrues.length;
}

//Task4
function doubleFactorial(n) {
    if(n < 0 || isNaN(n)) {
        return "The double factorial can be calculated only for positive integers.";
    } else if(n <= 1) {
        return 1;
    } else {
        return n * doubleFactorial(n-2);
    }
}

//Task5.1
function compareAge(person1, person2) {
    if(person1.age > 0 && person1.age <= 150 && person2.age > 0 && person2.age <= 150) {
        if(person1.age > person2.age) {
            return `${person1.name} is older than ${person2.name}`;
        } else if (person1.age < person2.age) {
            return `${person1.name} is younger than ${person2.name}`;
        } else {
            return `${person1.name} is the same age as ${person2.name}`;
        }
    } else {
        return "Person age should be a positive number between 1 and 150";
    }
}

//Task5.2
//Task 5.2.a Sort ascending
function sortPeopleAscendingByAge(arrOfPeople) {
    arrOfPeople.sort(function(person1, person2) {
        return person1.age - person2.age;
    });
    return arrOfPeople;
}

//Task 5.2.b Sort descending
function sortPeopleDescendingByAge(arrOfPeople) {
    arrOfPeople.sort((person1, person2) => person2.age - person1.age)
    return arrOfPeople;
}